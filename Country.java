package com.hmkcode;

import static org.apache.commons.lang3.StringUtils.EMPTY;
public class Country {

	private String nin;
	private String nameEN;
	private String nameAR;
	private String personalIDExpiry;
	private String personalID;
	private String passportExpiry;
	private String passportID;
	private String birthDate;
	private String nationality;
	private String mobileNo;
	private String address;
	private String phoneNumber;
	private String email;
	private String linkedNin;
	private String pobox;
	private String workPosition;
	private String workPlace;
	private String annualIncome;
	private String monthlyIncome;
	private String iban;
	private String otherSourceOfIncome;
	private String maintainanceMargin = EMPTY;
	private String marginCeiling = EMPTY;
	private String marginPercentage = EMPTY;
	private String gracePeriod = EMPTY;
	private String code;
	private String name;
    private String url;
    
    public Country(String nin, String nameEN, String nameAR, String personalIDExpiry, String personalID, String passportExpiry, String passportID, String birthDate, String nationality, String mobileNo, String address, String phoneNumber, String email, String linkedNin, String pobox, String workPosition, String workPlace, String annualIncome, String monthlyIncome, String iban, String otherSourceOfIncome, String maintainanceMargin, String marginCeiling, String marginPercentage, String gracePeriod) {
        this.nin = nin;
        this.nameEN = nameEN;
        this.nameAR = nameAR;
        this.personalIDExpiry = personalIDExpiry; 
		this.personalID = personalID; 
		this.passportExpiry = passportExpiry; 
		this.passportID = passportID; 
		this.birthDate = birthDate;
		this.nationality = nationality;
		this.mobileNo = mobileNo;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.linkedNin = linkedNin;
		this.pobox = pobox;
		this.workPosition = workPosition; 
		this.workPlace = workPlace;
		this.annualIncome = annualIncome;
		this.monthlyIncome = monthlyIncome;
		this.iban = iban;
		this.otherSourceOfIncome = otherSourceOfIncome; 
//		this.maintainanceMargin = maintainanceMargin;
//		this.marginCeiling = marginCeiling;
//		this.marginPercentage = marginPercentage;
//		this.gracePeriod = gracePeriod;
    }

    public Country(String code, String name, String url) {
        this.code = code;
        this.name = name;
        this.url = url;
    }
    
    public Country(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
	 * @return the nin
	 */
	public String getNin() {
		return nin;
	}

	/**
	 * @param nin the nin to set
	 */
	public void setNin(String nin) {
		this.nin = nin;
	}

	/**
	 * @return the nameEN
	 */
	public String getNameEN() {
		return nameEN;
	}

	/**
	 * @param nameEN the nameEN to set
	 */
	public void setNameEN(String nameEN) {
		this.nameEN = nameEN;
	}

	/**
	 * @return the nameAR
	 */
	public String getNameAR() {
		return nameAR;
	}

	/**
	 * @param nameAR the nameAR to set
	 */
	public void setNameAR(String nameAR) {
		this.nameAR = nameAR;
	}

	/**
	 * @return the personalIDExpiry
	 */
	public String getPersonalIDExpiry() {
		return personalIDExpiry;
	}

	/**
	 * @param personalIDExpiry the personalIDExpiry to set
	 */
	public void setPersonalIDExpiry(String personalIDExpiry) {
		this.personalIDExpiry = personalIDExpiry;
	}

	/**
	 * @return the personalID
	 */
	public String getPersonalID() {
		return personalID;
	}

	/**
	 * @param personalID the personalID to set
	 */
	public void setPersonalID(String personalID) {
		this.personalID = personalID;
	}

	/**
	 * @return the passportExpiry
	 */
	public String getPassportExpiry() {
		return passportExpiry;
	}

	/**
	 * @param passportExpiry the passportExpiry to set
	 */
	public void setPassportExpiry(String passportExpiry) {
		this.passportExpiry = passportExpiry;
	}

	/**
	 * @return the passportID
	 */
	public String getPassportID() {
		return passportID;
	}

	/**
	 * @param passportID the passportID to set
	 */
	public void setPassportID(String passportID) {
		this.passportID = passportID;
	}

	/**
	 * @return the birthDate
	 */
	public String getBirthDate() {
		return birthDate;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the mobileNo
	 */
	public String getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo the mobileNo to set
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the linkedNin
	 */
	public String getLinkedNin() {
		return linkedNin;
	}

	/**
	 * @param linkedNin the linkedNin to set
	 */
	public void setLinkedNin(String linkedNin) {
		this.linkedNin = linkedNin;
	}

	/**
	 * @return the pobox
	 */
	public String getPobox() {
		return pobox;
	}

	/**
	 * @param pobox the pobox to set
	 */
	public void setPobox(String pobox) {
		this.pobox = pobox;
	}

	/**
	 * @return the workPosition
	 */
	public String getWorkPosition() {
		return workPosition;
	}

	/**
	 * @param workPosition the workPosition to set
	 */
	public void setWorkPosition(String workPosition) {
		this.workPosition = workPosition;
	}

	/**
	 * @return the workPlace
	 */
	public String getWorkPlace() {
		return workPlace;
	}

	/**
	 * @param workPlace the workPlace to set
	 */
	public void setWorkPlace(String workPlace) {
		this.workPlace = workPlace;
	}

	/**
	 * @return the annualIncome
	 */
	public String getAnnualIncome() {
		return annualIncome;
	}

	/**
	 * @param annualIncome the annualIncome to set
	 */
	public void setAnnualIncome(String annualIncome) {
		this.annualIncome = annualIncome;
	}

	/**
	 * @return the monthlyIncome
	 */
	public String getMonthlyIncome() {
		return monthlyIncome;
	}

	/**
	 * @param monthlyIncome the monthlyIncome to set
	 */
	public void setMonthlyIncome(String monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	/**
	 * @return the iban
	 */
	public String getIban() {
		return iban;
	}

	/**
	 * @param iban the iban to set
	 */
	public void setIban(String iban) {
		this.iban = iban;
	}

	/**
	 * @return the otherSourceOfIncome
	 */
	public String getOtherSourceOfIncome() {
		return otherSourceOfIncome;
	}

	/**
	 * @param otherSourceOfIncome the otherSourceOfIncome to set
	 */
	public void setOtherSourceOfIncome(String otherSourceOfIncome) {
		this.otherSourceOfIncome = otherSourceOfIncome;
	}

	/**
	 * @return the maintainanceMargin
	 */
	public String getMaintainanceMargin() {
		return maintainanceMargin;
	}

	/**
	 * @param maintainanceMargin the maintainanceMargin to set
	 */
	public void setMaintainanceMargin(String maintainanceMargin) {
		this.maintainanceMargin = maintainanceMargin;
	}

	/**
	 * @return the marginCeiling
	 */
	public String getMarginCeiling() {
		return marginCeiling;
	}

	/**
	 * @param marginCeiling the marginCeiling to set
	 */
	public void setMarginCeiling(String marginCeiling) {
		this.marginCeiling = marginCeiling;
	}

	/**
	 * @return the marginPercentage
	 */
	public String getMarginPercentage() {
		return marginPercentage;
	}

	/**
	 * @param marginPercentage the marginPercentage to set
	 */
	public void setMarginPercentage(String marginPercentage) {
		this.marginPercentage = marginPercentage;
	}

	/**
	 * @return the gracePeriod
	 */
	public String getGracePeriod() {
		return gracePeriod;
	}

	/**
	 * @param gracePeriod the gracePeriod to set
	 */
	public void setGracePeriod(String gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Country{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
